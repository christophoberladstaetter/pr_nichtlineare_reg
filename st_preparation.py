# -*- coding: utf-8 -*-
import numpy as np
# ----------------------------------------------------------------------------------------------------------
# Globale Einstellungen für die physikalische Simulation für 4-Tanksystem bestehend aus Versuchsstand 2 u. 3
# ----------------------------------------------------------------------------------------------------------
# Dabei ist der obere Versuchsstand V3 und der untere V2
# Bei den Ventilabflüssen wird folgende Nomenklatur verwendet: 
# Prefix immer AS dann zwei Zahlen: erste Zahl = welcher Tank, zweite Zahl = welches Ventil

# Pumpe
Ku1 = 2.1e-5                 # m**3 / (s V) - Verstärkung der Pumpe 1
Ku2 = 2.3e-5                 # m**3 / (s V) - Verstärkung der Pumpe 2
uA01 = 7.0                  # V - Spannung ab welcher die Pumpe 1 Wasser fördert 
uA02 = 6.0                  # V - Spannung ab welcher die Pumpe 2 Wasser fördert 
uAmax1=12.0                 # Maximalspannung Pumpe 1
uAmax2=12.0                 # Maximalspannung Pumpe 2

# Tank 1 (Tank 1 V3)
rT1 = 0.060195545420039      # m - effektiver Radius des Tank 1 V2
AT1 = np.pi * rT1 ** 2       # m**2 - Tankquerschnitt 1
AS13 = 3.5e-05          # m**2 - Abflussquerschnitt von Tank 1 - Ventil11 zu Tank 3 mit 2.75 Umdrehung geschlossen
AS12 = 1e-05          # m**2 - Abflussquerschnitt von Tank 1 - Ventil12 zu Tank 2 mit 2.75 Umdrehung geschlossen
hV1 = 0.055                  # m - Höhe zwischen Nullniveau und Ventil
hT1 = 0.3                    # m - Höhe Tank 1

# Tank 2 (Tank 1 V2)
rT2 = 0.060195545420039      # m - effektiver Radius des Tank 1 V3
AT2 = np.pi * rT2 ** 2       # m**2 - Tankquerschnitt 2
hV2 = 0.055                  # m - Höhe zwischen Nullniveau und Ventil
hT2 = 0.3                    # m - Höhe Tank 2
AS23 = 1.5e-05          # m**2 - Abflussquerschnitt von Tank 2 - Ventil11 zu Tank 3 mit 2.00 Umdrehung geschlossen
AS24 = 3e-05          # m**2 - Abflussquerschnitt von Tank 2 - Ventil12 zu Tank 4 mit 2.75 Umdrehung geschlossen

# Tank 3 (Tank 2 V3)
rT3 = 0.060195545420039      # m - effektiver Radius des Tank 2 V2
AT3 = np.pi * rT3 ** 2       # m**2 - Tankquerschnitt 3
hV3 = 0.055                  # m - Höhe zwischen Nullniveau und Ventil
hT3 = 0.3                    # m - Höhe Tank 3
AS34 = 1e-05          # m**2 - Abflussquerschnitt von Tank 3 - Ventil22 zu Tank 4 mit 2.00 Umdrehungen geschlossen
AS30 = 3e-05          # m**2 - Abflussquerschnitt von Tank 3 - Ventil21 zu Reservoir mit 2.75 Umdrehungen geschlossen

# Tank 4 (Tank 2 V2)
rT4 = 0.060195545420039      # m - effektiver Radius des Tank 2 V3
AT4 = np.pi * rT4 ** 2       # m**2 - Tankquerschnitt 4
hV4 = 0.055                  # m - Höhe zwischen Nullniveau und Ventil zwischen Tank 4 und Reservoir
hT4 = 0.3                    # m - Höhe Tank 4
AS40 = 4e-05          # m**2 - Abflussquerschnitt von Tank 4 - Ventil22 zu Reservoir mit 2.00 Umdrehungen geschlossen

g = 9.81                     # m / s**2 - Erdbeschleunigung

scale = 2                    # Skalierung für die Visualisierung


initial_state = [0, 0, 0, 0]
