notebook
numpy
matplotlib
torch
scipy
imageio
scikit-learn
hmmlearn
torchvision
pandas
sympy
linalg